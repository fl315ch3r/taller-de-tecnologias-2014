package uy.edu.cei.taller2;

import org.springframework.stereotype.Service;

@Service(value = "bufon")
public class Bufon implements AlgoMuyTonto {

	public void diAlgo(String algoNoMuyInteligente) {
		System.out.println(algoNoMuyInteligente);
	}

}
