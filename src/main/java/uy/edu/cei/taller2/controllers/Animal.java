package uy.edu.cei.taller2.controllers;

import java.io.Serializable;

/**
 * DTO Animal, representa un animal.
 * 
 * @author fvillegas
 *
 */
public class Animal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1519083359954361991L;

	private String nombre;
	private String raza;

	public Animal() {
		// TODO Auto-generated constructor stub
	}

	public Animal(String nombre, String raza) {
		super();
		this.nombre = nombre;
		this.raza = raza;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", raza=" + raza + "]";
	}

}
