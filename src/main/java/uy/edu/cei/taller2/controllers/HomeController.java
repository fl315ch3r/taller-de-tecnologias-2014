/**
 * 
 */
package uy.edu.cei.taller2.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Esto es javadoc
 * 
 * @author fvillegas
 */
@Controller
@RequestMapping(value = "/algo")
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * http://algo/
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();

		Animal animal = new Animal("hipopotamo", "mamifero");
		Tool tool1 = new Tool("hammer", "punch");
		Tool tool2 = new Tool("hammer", "punch");

		List<Tool> tools = new ArrayList<Tool>();
		tools.add(tool1);
		tools.add(tool2);

		mav.addObject("animal", animal);
		mav.addObject("herramientas", tools);

		return mav;
	}

	/**
	 * http://algo
	 * payload {"nombre": "algo","raza":"algo"}
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public ModelAndView save(@RequestBody final Animal animal) throws IOException {
		ModelAndView mav = new  ModelAndView();
		
		logger.info("---> {}", animal);

		mav.addObject("animal", animal);
		
		return mav;
	}

}
