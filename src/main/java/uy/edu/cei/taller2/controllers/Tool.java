package uy.edu.cei.taller2.controllers;

import java.io.Serializable;

class Tool implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1015655661536303570L;

	private String name;
	private String type;

	public Tool() {
		// TODO Auto-generated constructor stub
	}
	
	public Tool(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}