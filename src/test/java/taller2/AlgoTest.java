package taller2;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uy.edu.cei.taller2.AlgoMuyTonto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/application-config.xml")
public class AlgoTest {

    @Resource(name ="bufon")
	private AlgoMuyTonto algoMuyTonto;
	
	@Test
	public void test() {
		this.algoMuyTonto.diAlgo("Hola");
	}
	
}
